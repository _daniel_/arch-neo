all:
	docker pull archlinux:base
	docker build -t arch-neo ./
	mkdir -p out
	docker run --rm --privileged --cap-add=sys_admin --security-opt label:disable --mount type=bind,source="$$(pwd)"/out,target=/root/out arch-neo make
