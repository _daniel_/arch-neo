FROM archlinux:base-20220703.0.65849
RUN pacman --noconfirm -Syu
RUN pacman -S --noconfirm archiso git base-devel
RUN useradd build
RUN mkdir /root/out
COPY src /root/
WORKDIR /root
#RUN make
